var FIRSTPAGEONLY = false;

var async = require('async');
var rest = require('restler');
var finder = require('./finder.js');
var util = require('./util.js');
var fs = require('fs');

/** Collections */
var music = util.db.collection('music');
var shorturls = util.db.collection('shorturls');
var songurls = util.db.collection('songurls');

/** Push onto here URLs to process. */
var liistenQ = async.queue(finder.findLinks, 1);

/** Push onto here MP3s to load into memory. */
var mp3Q = async.queue(processSong, 5);

/** 
 * The regexps for liisten links.
 */
//var artistExp = new RegExp('<h3>\\s*<a href="http://liisten\\.com/[a-z]*', 'gim');
var artistExp = new RegExp('<h3>\\s*<a href="http://liisten\\.com/[a-z]*">[A-Za-z\\s]*</a>', 'gim');
artistExp.compile(artistExp);
var titleExp = new RegExp('<a href="#play".*</a>', 'gim');
titleExp.compile(titleExp);
var stringExp = new RegExp('[A-Za-z]+(\\s[A-Za-z]+)?');
stringExp.compile(stringExp);

/** PAGE SRC */
var source = fs.readFileSync('liistensource.html');

/** The directory to save into. L suffix for liisten. */
var today = new Date();
var dirname = today.getFullYear() + '_' + today.getMonth() + '_' +
  today.getDate() + '_l';

function processSong(data, cb) {
  // Check if duplicate, find shorturl, match shorturl, add song entry, and
  // return the filepath and the url.
  console.log('Processing song:', data.url);
  songurls.find({ url: data.url }).toArray(function(err, song) {
    if (err) {
      console.log(err);
      cb(err);
    } else if (song.length == 0) {
      rest.get(data.url, { decoding: 'buffer' }).on('complete', function(buffer) {
        if (buffer instanceof Error) {
          console.log('Error in decoding', data.url);
          cb(buffer);
        } else {
          songurls.insert({ url: data.url });
          shorturls.findAndModify({ songId: null }, {},
            { $set: { songId: 'updating' } },
            function(err, shorturl) {
              if (err) {
                console.log('Error in finding shorturl.');
                cb(err);
              } else {
                console.log('Currently matching:', shorturl.url);
                var filepath = dirname + '/' + shorturl.url + '.mp3';
                
                // Check dups.
                music.find({ artist: data.artist, title: data.title }).toArray(
                  function(err, result) {
                    if (err) {
                      console.log(err);
                      cb(err);
                    } else if (result.length == 0) {
                      music.insert(
                        {
                          title: data.title,
                          artist: data.artist,
                          filename: filepath,
                          shorturl: shorturl.url,
                          random: Math.random()
                        },
                        { safe: true },
                        function(err, insertedSong) {
                          if (err) {
                            console.log(err);
                            console.log('RESETTING SHORTURL:', shorturl.url);
                            shorturls.updateById(
                              shorturl._id,
                              { $set: { songId: null } }
                            );
                            cb(err);
                          } else {
                            console.log('Now updating shorturl:', shorturl.url);
                            var songid = insertedSong[0]._id.toString();
                            
                            // Update shorturl's songId field.
                            shorturls.updateById(
                              shorturl._id,
                              { $set: { songId: songid } },
                              function(err) {
                                if (err) {
                                  console.log(err);
                                  console.log('COULD NOT UPDATE SHORTURL');
                                  cb(err);
                                } else {
                                  // SUCCESS
                                  console.log('UPDATED SHORTURL', shorturl.url);
                                  cb(null, filepath, data.url);
                                }
                              }
                            );
                          }
                        }
                      );
                    } else {
                      // Song is a duplicate.
                      console.log(data.title, 'is already in the system');

                      console.log('RESETTING SHORTURL:', shorturl.url);
                      shorturls.updateById(
                        shorturl._id,
                        { $set: { songId: null } }
                      );
                      cb(err);
                    }
                  }
                );
              }
            }
          );
        }
      });
    } else {
      cb(new Error('Song already exists.'));
    }
  });
};

/** Checks a liisten page URL/PAGENUM for artist urls. */
function checkPage() {
  console.log('Checking albums page for artists.');
  var artistPage;
  // Find all artist pages (with MP3s).
  while (artistPage = artistExp.exec(source.toString())) {
    var match = artistPage[0].split('"');
    var artistString = match.pop();
    var artist = artistString.split('\n').pop().split('<').shift();
    artist = stringExp.exec(artist)[0];
    var purelink = match.pop();
    console.log('Pushed', purelink, 'to queue.');
    // Make sure to scrape all artist pages for songs.
    (function(a, p) {
      liistenQ.push(purelink, function(mp3s, titles) {
        console.log('RETRIEVED MP3S:\n', mp3s);
        console.log('TITLES:\n', titles);
        var php = [];
        var counter = 0;
        // Find all titles on the page.
        
        // Find a shorturl and generate an entry in the music collection for
        // each MP3.
        for (var i = 0; i < mp3s.length; i++) {
          var data = {
            url: mp3s[i],
            title: titles[i],
            artist: a
          };
          mp3Q.push(data,
            function(err, filepath, mp3url) {
              counter++;
              console.log(a, counter, mp3s.length);
              if (filepath && mp3url) {
                var mp3data = {};
                mp3data[filepath] = mp3url;
                php.push(mp3data);
                console.log('ADDED', mp3data, 'TO DB.');
              }
              // If all MP3s have been added to the DB, we can make dreamhost
              // download everything.
              if (counter == mp3s.length) {
                console.log('All MP3s added to database.');
                rest.post('http://data.mixest.com/d.php',
                  { data:
                    { val: JSON.stringify(php) }
                  }
                ).on('complete', function(phpres) {
                  console.log('POSTED TO DREAMHOST', phpres);
                });
              }
            }
          );
        }
      });
    })(artist, purelink);
  }
};

checkPage();
