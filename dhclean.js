var fs = require('fs');
var dhdata = fs.readFileSync('dh-8-14', 'utf8');
var util = require('./util.js');
console.log(dhdata);

var music = util.db.collection('music');
var shorturls = util.db.collection('shorturls');

var invalid = /([a-z0-9][a-z0-9][a-z0-9])\.mp3\s+[0-9][0-9]-[A-Za-z]+-2012\s[0-9][0-9]:[0-9][0-9]\s+([0-9]+\.)?[0-9]+(K|\s)/g

var invalidUrl;
while (invalidUrl = invalid.exec(dhdata)) {
  console.log(invalidUrl[1]);
  console.log(invalidUrl[0]);
  (function(url) {
    shorturls.findAndModify({ url: url }, {'_id': -1}, { $set: { songId: null }}, function(err, url) {
      console.log('successfully updated');
    });
    //music.remove({ shorturl: url });
  })(invalidUrl[1]);
}
