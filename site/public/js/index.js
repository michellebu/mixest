$(document).ready(function(){
  if (window.location.hash) {
  	window.location = 'http://www.mixest.com/' + window.location.hash.split('#').pop();
  }
  // Social bar hover actions.
  $('#lfm').hover(function() { $('#lfmtip').fadeToggle('fast'); });//, function() { $('#lfmtip').stop().fadeOut('fast'); });
  $('#fb').hover(function() { $('#fbtip').fadeToggle('fast'); });//, function() { $('#fbtip').stop().fadeOut('fast'); });
  $('#tw').hover(function() { $('#twtip').fadeToggle('fast'); });//, function() { $('#twtip').stop().fadeOut('fast'); });
  $('#mb').hover(function() { $('#mbtip').fadeToggle('fast'); });//, function() { $('#mbtip').stop().fadeOut('fast'); });
  
  // Setting up local vars.
  $('.playlistsong').live('click', function() {
    var song = $(this).attr('id');
    playSong(song);
  });
  var recentlyp = JSON.parse(localStorage.getItem('recentlyp')) || [];
  handleHistory(true);
  var moreObscured = JSON.parse(localStorage.getItem('moreObscured')) || [];
  var songIndex = songs.length > 1 ? 0 : -1;
  if (songIndex == -1) {
	$('#playlist').css('display', 'none');
  } else {
  	$('#recent').css('display', 'none');
  	var allurls = '';
  	for (var i = 0; i < songs.length; i++) {
  		var song = songs[i];
		var div = i % 2 == 0 ? 'evenp' : 'oddp';
		var pentry = $('<a class="playlistsong" id="' + song._id + '"><div class="' + div + '">' + song.title + '</div>');
		$('#playlistcontainer').append(pentry);
		allurls += song.shorturl;
	}
	$('#playlistcontainer').append('<br><div class="padding"><input type="text" readonly="readonly" id="playlisturl"></input><center><button id="playlistoff">' +
		'Exit Playlist</button></center></div>');

	$('#playlisturl').val('http://www.mixest.com/' + allurls);
  }
  var currentSong = songs[0];
  if (sk) {
    localStorage.setItem('sk', sk);
    $('#lfmtip').html('You are already logged<br>in and scrobbling!');
  }
  var loves = JSON.parse(localStorage.getItem('loves')) || [];
  var lovesIds = JSON.parse(localStorage.getItem('lovesIds')) || [];
  $('#numberofhearts').html(loves.length);
  var heards = JSON.parse(localStorage.getItem('heards')) || [];

  var updateAll = function(song) {
    // TODO: do all songtitle stuff on backend.
    var songtitle = song.title || 'Untitled';
    var songartist = song.artist || 'Anonymous';
    $('#song-title').html(songtitle);
    $('#song-artist').html(songartist);
    $('#shorturl').val('http://www.mixest.com/' + song.shorturl);
    $('#reporttitle').val(songtitle);
    $('#reportartist').val(songartist);
    $('#reportsong').stop().fadeOut('fast');
    if (lovesIds.indexOf(currentSong._id) >= 0) {
      $('#lovebutton').stop().animate({ 'opacity': 1 });
    } else {
      $('#lovebutton').stop().animate({ 'opacity': 0.6 });
    }
    $('title').html(songtitle + ' - ' + songartist);
  }

  // For history.
  window.onpopstate = function(e) {
    if (e.state) {
      playSong(e.state.songId, true);
    }
  }
  var pushToHistory = function(song) {
    if (window.history.pushState) {
      window.history.pushState({'songId': song._id}, '', song.shorturl);
    }
  }
  
  // For lastfm scrobbling.
  function startScrobble() {
    $.post('/proxy', { sk: sk, update: true, artist: currentSong.artist || 'Anonymous', title: currentSong.title || 'Untitled' }, function(res) {
      console.log(res.success);
      if (res.success == '0') {
        $('#lfmtip').fadeIn('slow');
        $('#lfmtip').html('Whoops, we messed up.<br>Please sign in again!')
      }

    });
  }
  function endScrobble() {
    $.post('/proxy', { sk: sk, artist: currentSong.artist || 'Anonymous', title: currentSong.title || 'Untitled' }, function(res) {
      console.log(res);
    });
  }
  // Pushes a new song to history and takes care of the current history stack.
  function handleHistory(opt_load) {
    if (!opt_load) {
      var old = { title: currentSong.title,
        artist: currentSong.artist,
        id: currentSong._id };
      recentlyp.unshift(old);

      if (recentlyp.length == 12) {
        recentlyp.pop();
      }
    }

    for (var i = 0; i < recentlyp.length; i ++) {
      showSomeHistory(recentlyp[i], i+1);
    }

    localStorage.setItem('recentlyp', JSON.stringify(recentlyp));
  }

  // Plays a new song.
  function playNextSong(invalid) {
    // If we have a playlist, then go through the playlist.
    if (songIndex >= 0) {
      songIndex++;
      currentSong = songs[songIndex % songs.length];
      pushToHistory(currentSong);
      updateAll(currentSong);
      if (sk) { startScrobble(); }
      $('#player').jPlayer(
        'setMedia', 
        { mp3: 'http://data.mixest.com/' + currentSong.filename }).jPlayer('play');
      return;
    }
    // TODO: get rid of this ghettoness
    $.getJSON('/nextsong', { 'heards': moreObscured }, function(res) {
      if (!res.mp3) { playNextSong(true); return; }
      
      if (heards.indexOf(res.mp3[0]._id) >= 0) {
        playNextSong(true);
        return;
      }

      if (!invalid) {
        handleHistory();
      }

      currentSong = res.mp3[0];
      pushToHistory(currentSong);
      updateAll(currentSong);
      if (sk) { startScrobble(); }
      $('#player').jPlayer(
        'setMedia', 
        { mp3: 'http://data.mixest.com/' + currentSong.filename }).jPlayer('play');
    });
  }

  // Plays given song.
  function playSong(songId, isInHistory) {
    handleHistory();

    $.getJSON('/selectsong', { 'song': songId }, function(res) {
      currentSong = res.mp3;
      if (!isInHistory) { pushToHistory(currentSong); }
      updateAll(currentSong);
      if (sk) { startScrobble(); }
      $('#player').jPlayer(
        'setMedia',
        { mp3: 'http://data.mixest.com/' + currentSong.filename }).jPlayer('play');
    });

  }

  // JPlayer
  $('#player').jPlayer({
    ready: function () {
	  if (!currentSong) {
	    playNextSong(true);
	  } else {
            if (sk) { startScrobble(); }
            $(this).jPlayer('setMedia', {
              mp3: 'http://data.mixest.com/' + currentSong.filename }).jPlayer('play');
	    pushToHistory(currentSong);
	    updateAll(currentSong);
	  }
    },
    swfPath: '/js',
    supplied: 'mp3',
    ended: function() {
      if (sk) { endScrobble(); }
      playNextSong();
    }
  });

  // UI updating functions
  function showSomeLove(somelove, opt_num) {
    var songtitle = somelove.title || 'Untitled';
    var num = loves.length
    if (opt_num) { num = opt_num; }
    var div = num % 2 == 0 ? 'evenl' : 'oddl';
    var loveEntry = $('<div class="' + div + '"><div class="lovesong" id="' 
      + somelove.id + '">' + somelove.title + '</div><a id="' + somelove.id + '" class="delete">x</a></div>');
    if (num == 1) {
    	$('#lovescontainer').html(loveEntry);
    } else {
    	$('#lovescontainer').append(loveEntry)
    }
    $('#numberofhearts').html(loves.length);
  }
  function showSomeHistory(somehistory, opt_num) {
    var songtitle = somehistory.title || 'Untitled';
    var num = opt_num || recentlyp.length;
    var div = num % 2 == 0 ? 'even' : 'odd';
    var oldEntry = $('<a class="oldsong" id="' 
      + somehistory.id + '"><div class="' + div + '">' + songtitle + '</div></a>');
    if (num == 1) {
    	$('#recentcontainer').html(oldEntry);
    	return;
    }
    $('#recentcontainer').append(oldEntry);
  }

  for (var i = 0; i < loves.length; i += 1) {
    var love = loves[i];
    showSomeLove(love, i + 1);
  }

  // Handles love button.
  $('#lovebutton').click(function() {
    // Show message and add the song to localStorage.
    var newLove = {
      title: currentSong.title,
      artist: currentSong.artist,
      id: currentSong._id
    };
    if (lovesIds.indexOf(newLove.id) < 0) {
      loves.push(newLove);
      lovesIds.push(newLove.id);
      localStorage.setItem('loves', JSON.stringify(loves));
      localStorage.setItem('lovesIds', JSON.stringify(lovesIds));
      showSomeLove(newLove);
    }
    $(this).stop().animate({ 'opacity': 1 });
    $('#hearts').click();
  });

  // Handles sidebar triggers.
  if (songIndex == -1) {
  $('#hearts').click(function() {
    if ($('#loves').css('display') == 'none') {
    	$('#recent').stop().slideUp('fast', function() {
      		$('#loves').stop().slideDown('fast', function() {
      		});
    	});
    }
  });
  $('#recents').click(function() {
    if ($('#recent').css('display') == 'none') {
    	$('#loves').stop().slideUp('fast', function() {
      		$('#recent').stop().slideDown('fast', function() {
      		});
    	});
    }
  });
  }

  $('.lovesong, .oldsong').live('click', function() {
    var song = $(this).attr('id');
    playSong(song);
  });
  $('.delete').live('click', function() {
    var song = $(this).attr('id');
    var i = lovesIds.indexOf(song);
    loves.splice(i, 1);
    lovesIds.splice(i, 1);
    localStorage.setItem('loves', JSON.stringify(loves));
    localStorage.setItem('lovesIds', JSON.stringify(lovesIds));
    for (var i = 0; i < loves.length; i += 1) {
      var love = loves[i];
      showSomeLove(love, i + 1);
    }
    if (i == 0) {
      $('#lovescontainer').html("<div class='padding'>You haven't <strong>loved</strong> any songs yet! Poor songs.</div>");
    }
    $('#numberofhearts').html(loves.length);
  }); 

  // Handles context buttons.
  $('#share').click(function() {
    if ($('#sharelink').css('display') != 'none') {
    	$('#shorturl').stop().fadeOut('fast', function() {
    		$('#sharelink').stop().slideUp('fast');
    	});
      return;
    }
	  $('#reportform').stop().fadeOut('fast', function() {
		  $('#reportsong').stop().slideUp('fast', function() {
        $('#sharelink').stop().slideDown('fast', function() {
      	  $('#shorturl').stop().fadeIn('fast');
        });
		  });
	  });
  });
  $('#report').click(function() {
	  $('#approved').stop().hide();
	  if ($('#reportsong').css('display') != 'none') {
		  $('#reportform').stop().fadeOut('fast', function() {
			  $('#reportsong').stop().slideUp('fast');
		  });
		  return;
	  }
	  $('#shorturl').stop().fadeOut('fast', function() {
		  $('#sharelink').stop().slideUp('fast', function() {
	      $('#reportsong').stop().slideDown('fast', function() {
	        $('#reportform').stop().fadeIn('fast');
	      });
		  });
	  });
  });
  $('#reportsubmit').click(function() {
          var artist = $('#reportartist').val() == currentSong.artist ? 'no_op_mixest' : $('#reportartist').val();
          var title = $('#reporttitle').val() == currentSong.title ? 'no_op_mixest' : $('#reporttitle').val();
	  var data = { songId: currentSong._id, titlechange: title, artistchange: artist }; 
	  $.post('/report', data);
	  $('#reportform').stop().fadeOut('slow', function() {
		  $('#approved').stop().fadeIn('slow');
	  });
  });
  $('#reportdelete').click(function() {
  	var data = { songId: currentSong._id }; 
  	$.post('/report', data);
  	$('#reportform').stop().fadeOut('slow', function() {
  		$('#approved').stop().fadeIn('slow');
  	});
  });

  // Player buttons
  $('#next').click(function() {
    playNextSong();
  });
  $('#heards').click(function() {
    heards.push(currentSong._id);
    localStorage.setItem('heards', JSON.stringify(heards));
    playNextSong();
  });
  // Exit playlist mode.
  $('#playlistoff').click(function() {
    songIndex = -1;
    $('#playlist').stop().slideUp('fast', function() {
    	$('#recent').stop().slideDown('fast');
    });
	$('#hearts').click(function() {
	if ($('#loves').css('display') == 'none') {
		$('#recent').stop().slideUp('fast', function() {
			$('#loves').stop().slideDown('fast', function() {
			});
		});
	}
	});
	$('#recents').click(function() {
	if ($('#recent').css('display') == 'none') {
		$('#loves').stop().slideUp('fast', function() {
			$('#recent').stop().slideDown('fast', function() {
			});
		});
	}
	});
    playNextSong();
  });

  // Keypress.
  var volume = 0.5;
  var playing = true;
  $('#play').click(function() {
    playing = true;
  });
  $('#pause').click(function() {
    playing = false;
  });
  $(window).keydown(function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 32) { // Spacebar
      if (playing) {
        $('#player').jPlayer('pause');
        playing = false;
      } else {
        $('#player').jPlayer('play');
        playing = true;
      }
    } else if (code == 39) { // Right
      playNextSong();
    } else if (code == 38) { // Up
      if (volume < 1) {
        volume += 0.1;
      }
      $('#player').jPlayer('volume', volume);
    } else if (code == 40) { // Down
      if (volume > 0) {
        volume -= 0.1;
      }
      $('#player').jPlayer('volume', volume);
    }

  });
});
