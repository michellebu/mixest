var express = require('express');
var fs = require('fs');
var app =  express.createServer();
var https = require('https');
var md5 = require('MD5');
var rest = require('restler');
var util = require('../util.js');
var async = require('async');

var APIKEY = 'ea2bbf5937afa4df0d33cbd19d91ee33';
var SECRET = '46d7ed1447d510fddee66383df2cae32';

var mongo = require('mongoskin');
var db = mongo.db('mongodb://localhost:27017/mixest');
var music = db.collection('music');

var allsongs;
var lastUpdated;

// Initialize main server
app.use(express.bodyParser());

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.get('/', function(req, res) {
  if (allsongs) {
    var rand = Math.floor(Math.random() * allsongs.length); 
    music.findById(allsongs[rand]._id, function(err, mp3) {
      res.render('home', { mp3: [mp3], sk: '' });
    });
  } else {
    music.find({}, { '_id': 1 }).toArray(function(err, result) {
      if (result) {
        allsongs = result;
        var rand = Math.floor(Math.random() * result.length); 
        music.findById(allsongs[rand]._id, function(err, mp3) {
          res.render('home', { mp3: [mp3], sk: '' });
        });
      }
    });
  }
});

app.get('/nextsong', function(req, res) {
  if (allsongs) {
    var rand = Math.floor(Math.random() * allsongs.length); 
    music.findById(allsongs[rand]._id, function(err, mp3) {
      res.send({ mp3: [mp3] });
    });
  } else {
    music.find({}, { '_id': 1 }).toArray(function(err, result) {
      if (result) {
        allsongs = result;
        var rand = Math.floor(Math.random() * result.length); 
	music.findById(allsongs[rand]._id, function(err, mp3) {
		res.send({ mp3: [mp3] });
	});
      }
    });
  }
});

app.get('/selectsong', function(req, res) {
	music.findById(req.query.song, function(err, result) {
		console.log(result);
		res.send({ mp3: result });
	});
});



app.post('/report', function(req, res) {
  // TODO better
  if (req.body.titlechange) {
    util.db.collection('corrections').insert({ assocId: req.body.songId, deltatitle: req.body.titlechange, deltaartist: req.body.artistchange });
  } else {
    util.db.collection('todelete').insert({ assocId: req.body.songId });
  }
});


app.post('/proxy', function(req, res) {
  var method = req.body.update ? 'track.updateNowPlaying' : 'track.scrobble';
  var artist = req.body.artist;
  var title = req.body.title;
  var currts = Math.round((new Date()).getTime() / 1000);
  var ts = req.body.update ? '' : 'timestamp' + currts;
  var tsurl = ts ? '&timestamp=' + currts : '';
  var signature = md5('api_key' + APIKEY + 'artist' + artist +
    'method' + method + 'sk' +
    req.body.sk + ts + 'track' + title + SECRET);
  var postUrl = 'http://ws.audioscrobbler.com/2.0/';
  var postData = req.body.update ? {
    api_key: APIKEY,
    api_sig: signature,
    artist: artist,
    method: method,
    sk: req.body.sk,
    track: title
  } : {
    api_key: APIKEY,
    api_sig: signature,
    artist: artist,
    method: method,
    sk: req.body.sk,
    timestamp: currts,
    track: title
  };
  console.log(postData);
  var resexp = /lfm status="ok"/g
  rest.post(
    postUrl,
    { data: postData }
  ).on('complete', function(data) {
    console.log(data);
    if (!resexp.exec(data)) {
      //res.send({ success: 0 });
      return;
    }
    //res.send({ success: 1 });
  }).on('error', function(err) {
    console.log(err);
  });
  res.send(200);
});


app.get('/lastfm', function(req, res) {
  var token = req.query.token;
  if (!token) {
    res.redirect('/');
  }
  var signature = md5('api_key' + APIKEY + 'methodauth.getSessiontoken' + token + SECRET);
  console.log(signature);
  rest.get(
    'http://ws.audioscrobbler.com/2.0/?method=auth.getSession&token=' + token + '&api_key=' + APIKEY + '&api_sig=' + signature).on('complete', function(response) {
      var key = response.split('key')[1].split('>').pop().split('<').shift();
      console.log(key);
      if (allsongs) {
        console.log('picking song');
        var rand = Math.floor(Math.random() * allsongs.length); 
        music.findById(allsongs[rand]._id, function(err, mp3) {
          res.render('home', { mp3: mp3, sk: key });
        });
      } else {
        console.log('finding songs');
        music.find({}, { '_id': 1 }).toArray(function(err, result) {
          if (result) {
	    allsongs = result;
	    var rand = Math.floor(Math.random() * result.length); 
	    music.findById(allsongs[rand]._id, function(err, mp3) {
	      res.render('home', { mp3: mp3, sk: key });
	    });
          }
        });
      }  
    }
  ).on('error', function(err) { console.log(err) });
});

// TO DEPRECATE
app.get('old/:shortlink', function(req, res) {
  var sl = req.params.shortlink;
  if (sl.length == 3) {
    music.findOne({ shorturl: sl}, function(err, result) {
      res.send({ mp3: result });
    });
  } else { 
    res.redirect('/');
  }
})

// Shorturl handling
app.get('/:shortlink', function(req, res) {
  // TODO: better error handling
  var sl = req.params.shortlink;
  if (sl.length == 3) {
    music.findOne({ shorturl: sl}, function(err, result) {
      res.render('home', { mp3: [result], sk: '' });
    });
  } else if (sl.length % 3 == 0) { 
    var q = async.queue(function(url, callback) {
      music.findOne({ shorturl: url}, function(err, result) {
        if (!err) {
          callback(result);
        } else {
          callback(null);
        }
      });
    }, 5);
    var counter = 0;
    var mp3s = [];
    for (var i = 0; i < sl.length; i += 3) {
      var currurl = sl.substr(i, 3);
      q.push(currurl, function(song) {
        if (song) {
          mp3s.push(song);
        }
        counter += 1;
        if (counter == sl.length / 3) {
          console.log('playlist success', mp3s);
          res.render('home', { mp3: mp3s, sk: '' });
        }
      });
    }
    // handle playlist using async.
  } else {
    res.redirect('/');
  }
});
// Start server
var port = 8004;
app.listen(port);
