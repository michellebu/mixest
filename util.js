var mongo = require('mongoskin');

// mixest database
var db = mongo.db('mongodb://localhost:27017/mixest');

// are we in development?
var INDEV = true;
var DEBUG = true;

// regular expression patterns.
var mp3Regex = new RegExp('https?://[a-zA-Z0-9%/\\._-\\s]+\\.mp3', 'gim');
var linkRegex = new RegExp('https?://[a-zA-Z0-9/\\.\\?=_-]+', 'gim');
//var invalidLinkRegex = new RegExp('https?://[a-zA-Z0-9/\\.\\?=_-]+(?:pdf|jpeg|jpg|js|css|mp3|png|gif)', 'i');
var invalidLinkRegex = new RegExp('https?://[a-zA-Z0-9/\\.\\?=_-]+/[a-zA-Z0-9_-]\\.[a-zA-Z0-9]', 'i');
var rssRegex = new RegExp('https?://[a-zA-Z0-9/\\._-]+/\\??(rss|feed|atom\\.xml)=?[a-zA-Z0-9/\\._-]*', 'gim');
// this currently covers tumblr and wordpress blogs. blogspot is weird and makes for too
// useless of a regex.
var nextPageRegex = new RegExp('(page|paged)(=|/)[0-9]+', 'gim');
mp3Regex.compile(mp3Regex);
linkRegex.compile(linkRegex);
invalidLinkRegex.compile(invalidLinkRegex);
rssRegex.compile(rssRegex);
nextPageRegex.compile(nextPageRegex);


/**
 * Strips url down to domain name.
 */
var stripUrl = function(url) {
  var splitted = url.split('/')[2];
  var moreCut = splitted.split('.');
  return moreCut[0] == 'www' ? splitted.substring(4) : splitted; 
};


// export
module.exports = {
  db: db,
  INDEV: INDEV,
  DEBUG: DEBUG,
  mp3Regex: mp3Regex,
  linkRegex: linkRegex,
  invalidLinkRegex: invalidLinkRegex,
  nextPageRegex: nextPageRegex,
  rssRegex: rssRegex,
  stripUrl: stripUrl
};
