var rest = require('restler');
var ID3 = require('id3');
var util = require('./util.js');
var async = require('async');

var INDEV = util.INDEV;
var DEBUG = util.DEBUG;
process.setMaxListeners(0);

// Databases
var db = util.db;
var music = db.collection('music');
var songUrls = db.collection('songurls');
var shortUrls = db.collection('shorturls');
var obscurity = db.collection('obscurity');

var mp3Regex = util.mp3Regex;
var nextPageRegex = util.nextPageRegex;

var globalQ = async.queue(download, 5);
//var scrapeQ = async.queue(scrape, 1);

var scrape = function(info, pestControl) {
  if (pestControl) {
    pestControl();
  }
  rest.get(info.link).on('complete', function(result) {
    if (result instanceof Error) {
      console.log('Error: ' + result.message);
      this.retry(5000); // try again after 5 sec
    } else {
      scrapeHtml(info.link, result, info.page);
    }
  });
}
 

/**
 * Scrapes the given HTML snippet. FINDNEXT determines whether or not to
 * search the snippet for more outlinks.
 */
var scrapeHtml = function(page, content, pagination) {
  // Create a foldername based on date.
  console.log('Now scraping ' + page + '. Please wait...');
  var today = new Date();
  var foldername = today.getFullYear() + '_' + today.getMonth() 
    + '_' + today.getDate();

  makeFolders(foldername, content, 0);	
  
  findOlderPosts(page, content, pagination);
};


/**
 * Takes in an HTML string and finds 'Older Posts' to scrape.
 */
var findOlderPosts = function(url, html, current) {
  var nextPage, nextNumber, nextInt;
  var numRegex = new RegExp('[0-9]+');
  numRegex.compile(numRegex);
  nextPageRegex.compile(nextPageRegex);
  while (nextPage = nextPageRegex.exec(html)) {
    nextNumber = numRegex.exec(nextPage[0]);
    nextInt = parseInt(nextNumber[0]);
    console.log(nextPage[0], nextNumber[0], current, nextInt > current);
    if (nextNumber && nextInt > current) {
      break;
    }
  }

  if (nextPage) {
    if (nextNumber && current < nextInt) {
      var validLink = 'http://' + util.stripUrl(url) + '/' + nextPage[0];
      var scrapeInfo = { link: validLink, page: nextInt };
      scrape(scrapeInfo);
    } else {
      console.log('No more pages.');
    }
  } else {
    console.log('No older pages.');
  }
};


/**
 * Manages foldernames and executes download.
 */
var makeFolders = function(foldername, html, index) {
  var match, matches;
  console.log('Parsing HTML...');
  // count each matching MP3 link.
  var totalMatches = 0;
  var matches = [];
  while (!!(match = mp3Regex.exec(html))) {
    matches.push({rawfile: match[0], foldername: foldername});
  }
  console.log(matches);
  // download each MP3 link and save the entry.
  var artists = [];
  var counter = 0;
  var phpData = [];
  globalQ.push(matches, function(err, filename, url) {
    if (err) { console.log(err); }
    counter += 1;
    console.log(counter, matches.length);
    // Keeps these to send to dreamhost for download.
    if (!err) {
      var data = {};
      data[filename] = encodeURI(url);
      phpData.push(data);
      console.log('Pushed', data);
    }

    // If we've finished downloading...
    if (counter == matches.length) {
      console.log('All songs in this set have cbed');
      console.log(JSON.stringify(phpData));
      // TODO: Make this code less shitty.
      postToDreamHost(phpData);
    }
  });
};

function postToDreamHost(data) {
      rest.post(
        'http://data.mixest.com/d.php',
        { data: { val: JSON.stringify(data) } }
      ).on('complete', function(response) {
        console.log('Posted to DREAMHOST', response);
        if (response instanceof Error) {
          setTimeout(function() { postToDreamHost(data); }, 5000);
        }
      });
};



/**
 * Download the MP3 from the RAWFILE url to the FOLDERNAME folder.
 */
function download(filedata, callback) {
  var rawfile = filedata.rawfile;
  console.log('DOWNLOADING: ', filedata.rawfile);
  if (rawfile == 'http://indiemuse.com/wp-content/uploads/2011/11/02%20The%20Ornament.mp3') { callback('ornament'); };
  var foldername = filedata.foldername;
  // checks db to see if we have already downloaded this MP3.
  songUrls.find({ url: rawfile }).toArray(function(err, unwanted) {
    if (err) {
      console.log(err);
      callback(err, null, null);
    } else if (unwanted.length == 0) {

      rest.get(rawfile, { decoding: 'buffer' } ).on('complete', function(res) {
        if (res instanceof Error) {
          // do nothing
          callback('could not open', null, null);
        } else {
          // saves url so that we don't download this file again.
          songUrls.insert({ url: rawfile });
          shortUrls.findAndModify({ songId: null }, {},
              { $set: { songId: 'updating' }}, function(err, someurl) {
            if (err) {
              // TODO: handle the running out of shorturls.
              callback('shorturls ran out', null, null);
            } else {
              console.log('Now matching ', someurl.url);
              var filename = foldername + '/' + someurl.url + '.mp3';
                  
              // parse the id3 tag.
              id3parse(res, filename, function(id3) {
                if (!id3) {
                  callback('could not get id3 info', null, null);
                }
                savedata(rawfile, id3, filename, callback, function(songEntry) {
                  var mp3raw = songEntry[0];
                  var songid = mp3raw._id.toString();
                  var urlid = someurl._id.toString();

                  console.log('Now updating ', someurl.url);
                              
                  // update song entry's shorturl.
                  music.updateById(
                    songid,
                    { $set: { shorturl: someurl.url } },
                    function(err, someSong) {
                      if (err) {
                        console.log(err);
                      } else {
                        // update shorturl entry's song.
                        console.log('SUCCESSFULLY UPDATED MUSIC WITH SHORTURL');
                        shortUrls.updateById(
                          urlid,
                          { $set: { songId: songid } },
                          function(err) {
                            if (err) {
                              console.log(err);
                            } else {
                              console.log('SHORTURL UPDATED WITH SONG');
                            }
                          }
                        );
                      }
                    });
                });
              });

            }
          });
        }
      });
    } else {
      console.log(rawfile + ' has already been downloaded. Skipping.');
      callback('dup', null, null);
    }
  });
};


/**
 * Parses the ID3 tag to gather MP3 information.
 * TODO: check for bad titles/artist names.
 */
var id3parse = function(data, filename, callback) {
  try {
    var id3 = new ID3(data);
    id3.parse();
    console.log(id3.get('title'), ' ', id3.get('artist'));
    // save the file data into the database.
    callback(id3);
  } catch (err) {
    console.log('INVALID MP3');
    callback(err);
  }
};


/**
 * Saves the MP3 data into the music database.
 * Checks if the song is a duplicate; if so, delete it from the FS.
 */
var savedata = function(rawfile, data, filename, callback, savesongurl) {
  checkDup(data, function(isOk) {
    if (isOk) {
      console.log('IS NOT A DUP, proceed');
      music.insert({
        title: data.get('title'),
        artist: data.get('artist'),
        album: data.get('album'),
        year: data.get('year'),
        filename: filename,
        shorturl: '',
        random: Math.random()
      }, { safe: true },

      // assigns shorturl to song.
      function (err, songEntry) {
        if (err) {
          console.log(err);
          callback('error', null, null);
        } else {
          savesongurl(songEntry);
          callback(null, filename, rawfile);
        }
      });
    } else {
      console.log('ITS A DUP GUYS', rawfile);
      callback('dup', null, null);
    }
  });
};


/**
 * Assigns shortcut to MP3.
 * DEPRECATED!
 */
var processShortUrl = function(mp3) {
  shortUrls.findAndModify({ song: null }, {} /* sort */,
      { $set: { songId: 'updating' }}, function(err, someurl) {
    if (err) {
      // TODO: handle the running out of shorturls.
    } else {
      var mp3raw = mp3[0];
      var songid = mp3raw._id.toString();
      var urlid = someurl._id.toString();

      console.log('Now matching ', someurl.url);
                  
      // update song entry's shorturl.
      music.updateById(
        songid,
        { $set: { shorturl: someurl.url } },
        function(err, someSong) {
          if (err) {
            console.log(err);
          } else {
            // update shorturl entry's song.
            shortUrls.updateById(
              urlid,
              { $set: { songId: songid } },
              function(err) {
                if (err) {
                  console.log(err);
                }
              }
            );
          }
        }
      );
    }
  });  
};


/** 
 * Adds artists from ARTISTS to the DB if they aren't already there with a neutral
 * obscurity.
 */
var bulkAddArtistObscurity = function(artists) {
  if (DEBUG) console.log('BULKADDARTISTOBSCURITY');
  for (var i = 0; i < artists.length; i += 1) {
    var artist = artists[i];
    obscurity.findOne({ artist: artist }, function(err, result) {
      if (!err && !result) {
        obscurity.insert({ artist: artist, obscurity: 0 }, { safe: true },
          function(err) {
            if (err) {
              console.log(err);
            }
          }
        );
      }
    });
  }
}


/** A check for duplicate songs. Returns false if the given data is not unique. */
var checkDup = function(data, callback) {
  music.find({ artist: data.get('artist'), title: data.get('title') }).
      toArray(function(err, result) {
    if (err) {
      console.log(err);
    } else {
      callback(result.length == 0);		
    }
  });
};


/**
 * Generates the shorturl at index I of length LEN.
 */
var genShortUrl = function(i, len) {
  var allChars = '0123456789abcdefghijklmnopqrstuvwxyz';
  var url = '';
  var numChars = allChars.length;
  while (i != 0) {
    url = allChars.charAt(i % numChars) + url;
    i = Math.floor(i / numChars);
  }
  while (url.length != len) {
    url = '0' + url;
  }
  return url;
};


/**
 * Saves all shorturls of length LEN into the database, assigning them to null songId.
 */
var saveShortUrls = function(len) {
  for (var i = 0; i < Math.pow(36, len); i += 1) {
    var currentUrl = genShortUrl(i, len);
    shortUrls.insert({
      url: currentUrl,
      songId: null
    });
  }
};


/**
 * The exposed scrape function.
 * Scrapes the html page, PAGE, for all MP3 links and creates a foldername based
 * on scrape date.
 * FINDNEXT is a boolean value that determines whether or not we should be looking for
 * 'older' or 'next' buttons so that we can scrape all pages.
 * PESTCONTROL is the callback function that disposes of the associated spider. We can
 * dispose of it before anything completes because we already have all the information
 * we need for the scrape.
 */
module.exports = {
  scrape: scrape,

  scrapeHtml: scrapeHtml,

  saveShortUrls: saveShortUrls,

  checkDup: checkDup
};
