// Requires here. I know you need them!
var feed = require('superfeedr').Superfeedr;
var scraper = require('./scraper.js');
var util = require('./util.js');

var client;
var db = util.db;
var INDEV = util.INDEV;
var DEBUG = util.DEBUG;
var rssRegex = util.rssRegex;
var linkRegex = util.linkRegex;
var invalidLinkRegex = util.invalidLinkRegex;

var blogs = db.collection('blogs');


/**
 * Initializes the feed and tells Superfeedr what to do with new entries.
 */
var initializeFeed = function() {
  //client = new feed(process.env.FEEDUSERNAME, process.env.FEEDPASSWORD);
  //client = new feed('michellebu', 'spoty73');
  //client.on('connected', function() {
    //if (DEBUG) { console.log('RSS client connected.'); }
    //client.on('notification', function(notification) {
      //var entries = notification.entries;
      //for (var i = 0; i < entries.length; i += 1) {
       // var entry = entries[i];
       // scraper.scrapeHtml(null, entry.content, 0);
      //}
    //});
  //});
};


/**
 * An eight-legged non-insect that can crawl.
 * Takes care of some HTML and is exterminated once it finishes the following:
 *     .subscribe() - subscribes to this URL.
 *     .getOutlinks() - returns all outlinks.
 */
var Spider = function(url, html) {
  this.link = url;
  this.content = html;
  this.rssFeed = null;

  //initializeFeed();
};


/** Find and subscribe to RSS feed. */
Spider.prototype.subscribe = function(rssFeed, link) {
  //client.on('connected', function() {
    if (DEBUG) { console.log('SUBSCRIBING TO: ', rssFeed); }
    if (!rssFeed) {
      return;
    }
    //client.subscribe(rssFeed /*secureLink(rssFeed)*/, function(err, feed) {
      //if (err) {
      //  console.log(err);
      //} else {
       // console.log(feed);
        var strippedUrl = util.stripUrl(link);
        // we need to save blog to db for the sake of initial scrape.
        blogs.insert({ url: strippedUrl }, function(err) {
          if (err) {
            console.log(err);
          }
        });
      //}
    //});
 // });
};


Spider.prototype.getOutlinksAndSubscribe = function(callback) {
  var temp = rssRegex.exec(this.content);
  this.rssFeed = temp ? temp[0] : null;
  if (DEBUG) { console.log('RSS FEED: ', this.rssFeed); }
  this.subscribe(this.rssFeed, this.link); 
  this.getOutlinks(callback);
};


/**
 * Get all outlinks in html.
 */
Spider.prototype.getOutlinks = function(callback) {
  var outlinks = [];
  var outlink;
  var devCount = 0;
  var allUrls = '';
  while(outlink = linkRegex.exec(this.content)) {
    if (INDEV && devCount > 10) { break; }
    // make sure the link we get is not in this spider's domain.
    var stripOL = util.stripUrl(outlink[0]);
    if (stripOL.indexOf(util.stripUrl(this.link)) == -1
        && allUrls.indexOf(stripOL) == -1
        && stripOL != 'adobe.com'
        && stripOL != 'facebook.com'
        && stripOL != 'youtube.com'
        && stripOL.indexOf('twitter.com') == -1
        && stripOL.indexOf('blogger.com') == -1
        && stripOL.indexOf('w3.org') == -1
        && stripOL != 'click.linksynergy.com'
        && stripOL.indexOf('tumblr.com') == -1
        && stripOL != 'feeds.feedburner.com'
        && stripOL != 'feeds2.feedburner.com'
        && stripOL != 'amazon.com'
        && stripOL != 'myspace.com'
        && stripOL != 'garageband.com'
        && stripOL != 'rapidshare.com'
        && stripOL != 'zonaindie.com.ar'
        && stripOL != 'zonaindie.com'
        && stripOL.indexOf('wordpress.org') == -1) {
      invalidLinkRegex.compile(invalidLinkRegex);
      if (!invalidLinkRegex.test(outlink[0]) && stripOL.split('.').length >= 2) {
        outlinks.push(outlink[0]);
        devCount += 1;
        allUrls += stripOL;
      }
    }
  }
  callback(this.link, outlinks);
};


/**
 * Given a LINK, returns the secure URL (mainly for RSS).
 */
var secureLink = function(link) {
  if (link.substring(0, 4) != 'http') {
    return 'https://' + link;
  }

  if (link.substring(0, 5) != 'https') {
    return 'https' + link.substring(4);
  }

  return link;
};


module.exports = {
  Spider: Spider
};

