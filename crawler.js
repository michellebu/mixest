// Do we want to scrape all blogs thoroughly?
var INITIAL_DISCOVER = true;

var rest = require('restler');
var async = require('async');

var scraper = require('./scraper.js');
var spider = require('./spider.js');
var util = require('./util.js');

var INDEV = util.INDEV;
var DEBUG = util.DEBUG;
var mp3Regex = util.mp3Regex;
var rssRegex = util.rssRegex;

// Databases (may not need this if RSS provides check).
var db = util.db;
var blogs = db.collection('blogs');

/**
 * Crawler that, given an Array of URLs to start crawling on,
 * will send spiders through each URL and their outlinks, subscribing
 * to relevant blogs and scraping their music.
 */
var crawl = function(urlArray, limit) {
  if (limit) {
    limit();
  }
  for (var i = 0; i < urlArray.length; i += 1) {
    var url = urlArray[i];
    (function (u /* the url */) {
      checkRelevance(u, function(isRelevant, html) {
        if (DEBUG) { console.log(u, ' IS RELEVANT? ', isRelevant); }
        if (isRelevant) {
          var spidey = new spider.Spider(u, html);
          // callback takes url to preserve source url.
          spidey.getOutlinksAndSubscribe(function(u, outlinks) {
            console.log('Now crawling ', u);
            // find all outlinks as well as rss feed.
            if (DEBUG) { console.log('GETOUTLINKS: ', outlinks); }

            scraper.scrape({ link:u, page: 1 }, function() { delete(spidey); });
            if (outlinks.length > 0) {
              // crawl through all outlinks. Most of the time affiliates are listed on
              // the homepage so we don't need to find the outlinks of other pages.
              if (DEBUG) { console.log('NOW SPIDERING OUTLINKS OF ', u); }
              crawl(outlinks, limit);
            }
          });
        }
      });
    })(url);
  }
};


/**
 * Checks the relevance of the given URL.
 * A URL is relevant to our interests if it has an RSS feed (is a blog)
 * and if it contains MP3 links on its frontpage (is a music blog).
 * Finally, a check that it is not already subscribed to is needed.
 * TODO: More sophisticated checks for INDIE music blog.
 */
var checkRelevance = function(url, callback) {
  rest.get(url).on('complete', function(result) {
    if (result instanceof Error) {
      console.log(result.message);
      this.retry(5000);
    } else {
      var strippedUrl = util.stripUrl(url);
      blogs.find({ url: strippedUrl }).toArray(function(err, matchedBlogs) {
        if (err) {
          console.log(err);
        } else {
          // if this blog is already in the db or not valid...
          if (matchedBlogs.length == 0 && isValidBlog(result)) {
            callback(true, result);
          } else {
            callback(false, null);
          }
        }
      });
    }
  });
};


/**
 * Checks if the HTML snippet passed in has A: an RSS feed, B: MP3s.
 */
var isValidBlog = function(html) {
  return mp3Regex.test(html) && rssRegex.test(html);
};


// Begin.
if (INDEV) {
  // array of blogs to crawl.
  var toScrape = [/*'http://3hive.com/'/*, done aug 9
    'http://apesforindie.wordpress.com/'/*, done aug 10
    'http://www.deafindieelephants.com/'/*, done aug 10
    'http://fiddlefreak.com/',
    'http://www.fuelfriendsblog.com/', frontpage done aug 10
    'http://www.indieball.com/'/*, done aug 10
    'http://www.musiclikedirt.com/ aug 10 
    'http://www.musicsavage.com/'??? */
   /*'http://quietcolor.com/qc/', has no rss, need to fix
    'http://www.thankscaptainobvious.net/' aug 14 
'http://blog.minneapolisfuckingrocks.com/', aug 14
'http://allthingsgomusic.com/', aug 14
'http://www.aquariumdrunkard.com/' aug 14
    'http://www.iguessimfloating.net/', aug 14
   'http://www.aerialnoise.com/', aug 14
'http://berkeleyplaceblog.com/', aug 14 
'http://www.anotherformofrelief.com' aug 14 */
    'http://www.friendswithbotharms.com',
    'http://www.fingertipsmusic.com/',
    'http://walrusmusicblog.com'
    /*
    'http://thosegeese.wordpress.com/', kinda bad
    'http://17seconds.co.uk/blog/' ???*/
    /*'http://www.indiemuse.com/page/2' done aug 14*/] 
  //scraper.saveShortUrls(3);
  var countCrawls = 0;
  crawl(toScrape, function() {
    if (countCrawls > 20) { return; }
    countCrawls += 1;
    console.log('CRAWLS: ', countCrawls);
  });
};
