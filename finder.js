var rest = require('restler');
var http = require('http');
var fs = require('fs');
var async = require('async');

var data = fs.readFileSync('template');

/*
rest.post('http://data.mixest.com/d.php', {data: {val: JSON.stringify([{"2012_6_21_47/01t.mp3":"http://glgpub.com.s120036.gridserver.com/_/audio/Gwyneth-and-Monko_Get-In-The-Sun.mp3 "}]
)}}).on('complete', function(res){

  console.log(res);

});*/

exports.findLinks = function (url, cb) {

  rest.get(url).on('complete', function(data){
  
    var patt = /id&quot;:&quot;([0-9]+)&quot;,&quot;uid&quot;:&quot;([0-9]+)&quot;/g;
    var titpatt = new RegExp('<a href="#play".*</a>', 'gim');
    titpatt.compile(titpatt);
    var val;
    var ids = [];
    var seen = {};
    while (val = patt.exec(data)) {
      if(!seen[val[2]]){
        ids.push({audioid: val[1], uid: val[2]});
        seen[val[2]] = true;
      }
    }
    var tit;
    var titles = [];
    while (tit = titpatt.exec(data)) {
      var postit = tit[0].split('</a>')[0].split('>').pop();
      if (postit != 'Play/Pause') {
        titles.push(postit);
      }
    }
    async.map(ids, function(el, cb){
      findLink(el.uid, el.audioid, function(link){
        cb (null, link)
      });
    }, function(err, results){
      cb(results, titles);
    });
  
  });

}

function findLink(uid, audioid, cb){
  var y = JSON.stringify({"type":"song","uid":uid,"audioId":audioid});
  var x = new Buffer(data.length + y.length);
  data.copy(x);
  x.write(y, data.length);

  var options = {headers:{}};
  options.host = 'virb.com';
  options.port = '80';
  options.path = '/services/flash/gateway';
  options.path = 'http://' + options.host + ':' + options.port + options.path;
  options.headers['content-length'] = x.length;
  options.method = 'POST';

  req = http.request(options, function (res) {
     var x = '';
     res.on('data', function(chunk){
      x += chunk;
     });
     res.on('end',function(){
       x = x.toString();
       var mp3 = x.substring(x.indexOf('http'), x.indexOf('.mp3')+4);
       cb(mp3);
     });
  });
  req.write(x);
  req.end();
}
